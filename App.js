/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import HomeNav from './js/navigators/HomeNav.js';
import Settings from './js/screens/Settings.js';
import Profile from './js/screens/Profile.js';
import SignIn from './js/screens/SignIn.js';
import SignUp from './js/screens/SignUp.js';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Title,
  Icon,
  Container,
} from 'native-base';
import  AsyncStorage from '@react-native-community/async-storage';
const loginStack = createStackNavigator();
const mainStack = createStackNavigator();
class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      isSignedIn : true,
      csrfToken: '',
      };
    this.signInCallback = this.signInCallback.bind(this);
    this.getToken = this.getToken.bind(this);
  }

  signInCallback(){
    this.setState({isSignedIn:true});
  }

  signOutCallback(){
    this.setState({isSignedIn:false});
  }

  componentDidMount(){
    // try {
    //   AsyncStorage.getItem('csrfToken', (error,value) => {
    //     if (!error && value !== null) {
    //       this.setState({csrfToken: value});
    //     }
    //     else {
    //       this.getToken();
    //     }
    //   });
    // }
    // catch (error) {
    //   console.error(error);
    // }
    this.getToken();
  }

  getToken(){
    try {
      fetch('http://192.168.99.100:8000/auth/signup/')
            .then(response => {
              console.log(response);
              var cookieString = response.headers.map['set-cookie'];
              var strings = cookieString.split(';');
              var tokenString = strings[0];
              var token = tokenString.split('=')[1];
              this.setState({csrfToken: token, cookie:cookieString});
              // AsyncStorage.setItem('csrfToken', token);
              console.log(token);

            })
            .catch(error => {
              console.log('error');
              console.log(error);
            });

    }
    catch (error){
      console.error(error);
      console.warn('unable to get or store token');
    }

  }

  render (){
    return (
      <NavigationContainer >
        {this.state.isSignedIn ? (
          <mainStack.Navigator>
            <mainStack.Screen
              name="Home"
              component={HomeNav}
              initialParams={{csrfToken: this.state.csrfToken}}
              options={{headerShown: false}}/>
            <mainStack.Screen
              name="Settings"
              component={Settings}
              initialParams={{csrfToken: this.state.csrfToken}}
              options={{title:<Title style={{color:'black'}}>SoCalender <Icon name="settings" /></Title>, headerTitleAlign: 'center'}}/>
            <mainStack.Screen
              name="Profile"
              initialParams={{signOutCallback: ()=> this.signOutCallback(), csrfToken: this.state.csrfToken}}
              component={Profile}
              options={{title:<Title style={{color:'black'}}>SoCalender <Icon name="person" /></Title>, headerTitleAlign: 'center'}}/>
            </mainStack.Navigator>
        ) : (
          <loginStack.Navigator>
            <loginStack.Screen
              name="SignIn"
              initialParams={{signInCallback: ()=> this.signInCallback(),
                csrfToken: this.state.csrfToken,
                cookie: this.state.cookie,
              }}
              component={SignIn}
              options={{title:<Title style={{color:'black'}}>SoCalender <Icon name="calendar" /></Title>, headerTitleAlign: 'center'}}/>
            <loginStack.Screen
              name="SignUp"
              initialParams={{signInCallback: ()=> this.signInCallback(), csrfToken: this.state.csrfToken}}
              component={SignUp}
              options={{title:<Title style={{color:'black'}}>SoCalender <Icon name="calendar" /></Title>, headerTitleAlign: 'center'}}/>
          </loginStack.Navigator>
        )}
      </NavigationContainer>
    );
  }
}
export default App;
