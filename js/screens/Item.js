/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  NativeModules,
  ScrollView,
  TextInput,
} from 'react-native';
import {Container} from 'native-base';
import AddButton from '../components/AddButton';
class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Callback: '',
      Edit: this.props.route.params?.edit ?? false,
      item: this.props.route.params?.item,
      styles: StyleSheet.create({
        field: {
          margin: 2,
        },
        input: {
          flex: 3,
          borderWidth: 2,
          borderColor: 'black',
          backgroundColor: 'lightgray',
          color: 'black',
        },
        label: {
          marginRight: 5,
          flex: 1,
          fontWeight: 'bold',
          textAlignVertical: 'center',
          textAlign: 'right',
        },
      }),
    };
    // this.addToCalendar = this.addToCalendar.bind(this);
    this.editPress = this.editPress.bind(this);
    this.savePress = this.savePress.bind(this);
  }

  // componentDidMount() {
  //   return fetch('')
  //     .then(response => response.json())
  //     .then(responseJson => {
  //       this.setState({isLoading: false, dataSource: responseJson.Activity});
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }

  // async load() {
  //   await ActivityLoader.testCallback(
  //     err => {
  //       console.log(err);
  //     },
  //     msg => {
  //       console.log(msg);
  //       return msg;
  //     },
  //   );
  // }
  savePress() {
    //Post event
    console.log('saving');
  }

  editPress() {
    if (this.state.Edit) {
      //save API post
      this.setState({
        Edit: false,
        styles: {
          field: {
            margin: 2,
          },
          input: {
            flex: 3,
            borderWidth: 2,
            borderColor: 'black',
            backgroundColor: 'lightgray',
            color: 'black',
          },
          label: {
            marginRight: 5,
            flex: 1,
            fontWeight: 'bold',
            textAlignVertical: 'center',
            textAlign: 'right',
          },
        },
      });
    } else {
      this.setState({
        Edit: true,
        styles: {
          field: {
            margin: 2,
          },
          input: {
            flex: 3,
            borderWidth: 2,
            borderColor: 'black',
            backgroundColor: 'white',
            color: 'black',
          },
          label: {
            marginRight: 5,
            flex: 1,
            fontWeight: 'bold',
            textAlignVertical: 'center',
            textAlign: 'right',
          },
        },
      });
    }
  }

  render() {
    this.props.navigation.setOptions({
      title: this.props.route.params.item.name,
    });
    return (
      <Container>
        <ScrollView>
          <View flexDirection="row" style={this.state.styles.field}>
            <Text style={this.state.styles.label}>Description:</Text>
            <TextInput
              editable={this.state.Edit}
              style={this.state.styles.input}
              defaultValue={this.state?.item?.description}
            />
          </View>
          <View flexDirection="row" style={this.state.styles.field}>
            <Text style={this.state.styles.label}>Date:</Text>
            <TextInput
              editable={this.state.Edit}
              style={this.state.styles.input}
              defaultValue={this.state.item?.date}
            />
          </View>
          <View flexDirection="row" style={this.state.styles.field}>
            <Text style={this.state.styles.label}>Start Time:</Text>
            <TextInput
              editable={this.state.Edit}
              style={this.state.styles.input}
              defaultValue={this.state.item?.startTime}
            />
          </View>
          <View flexDirection="row" style={this.state.styles.field}>
            <Text style={this.state.styles.label}>Time:</Text>
            <TextInput
              editable={this.state.Edit}
              style={this.state.styles.input}
              defaultValue={this.state.item?.endTime}
            />
          </View>
          <View flexDirection="row" style={this.state.styles.field}>
            <Text style={this.state.styles.label}>Location:</Text>
            <TextInput
              editable={this.state.Edit}
              style={this.state.styles.input}
              defaultValue={this.state.item?.location}
            />
          </View>
          <View flexDirection="row" style={this.state.styles.field}>
            <Text style={this.state.styles.label}>Links:</Text>
            <TextInput
              editable={this.state.Edit}
              style={this.state.styles.input}
              defaultValue={this.state.item?.links}
            />
          </View>
          <View flexDirection="row" style={this.state.styles.field}>
            <Text style={this.state.styles.label}>Participants:</Text>
            <TextInput
              editable={this.state.Edit}
              style={this.state.styles.input}
              defaultValue={this.state.item?.participants}
            />
          </View>
        </ScrollView>
        <AddButton
          icon={
            this.state.Edit
              ? this.props.route.params.saveIcon
              : this.props.route.params.editIcon
          }
          onPress={this.editPress}
        />
      </Container>
    );
  }
}
export default Item;
