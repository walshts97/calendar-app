/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, TextInput, ScrollView, FlatList} from 'react-native';
import DiscoveryBox from '../components/DiscoveryBox.js';
import {Container} from 'native-base';
import Woodys from '../../media/UncleWoodys.jpg';
import Reds from '../../media/Reds.jpg';
import Minigolf from '../../media/Minigolf.jpg';
import Question from '../../media/Question.png';
import Tree from '../../media/Tree.jpg';
import Concert from '../../media/Concert.jpg';

const data = {
  Routes: [
    {
      name: 'Discover',
      key: 0,
    },
    {
      name: 'Bars',
      key: 1,
    },
    {
      name: 'Sports',
      key: 2,
    },
    {
      name: 'Outdoors',
      key: 3,
    },
    {
      name: 'Concerts',
      key: 4,
    },
    {
      name: 'Family',
      key: 5,
    },
    {
      name: 'Other',
      key: 6,
    },
  ],
  Discover: [
    {
      image: Woodys,
      text: 'Bars',
      color: 'Black',
      key: 1,
      route: 'SubCategory',
    },
    {
      image: Reds,
      text: 'Sports',
      color: 'red',
      key: 2,
      route: 'SubCategory',
    },
    {
      image: Tree,
      text: 'Outdoors',
      key: 3,
      color: 'green',
      route: 'SubCategory',
    },
    {
      image: Concert,
      text: 'Concerts',
      key: 4,
      color: 'Blue',
      route: 'SubCategory',
    },
    {
      image: Minigolf,
      text: 'Family',
      key: 5,
      color: 'orange',
      route: 'SubCategory',
    },
    {
      image: Question,
      text: 'Other',
      key: 6,
      color: 'purple',
      route: 'SubCategory',
    },
  ],
  Bars: [
    {
      key: 0,
      text: 'woodys',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
    {
      key: 1,
      text: 'woodys',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
  ],
  Sports: [
    {
      key: 0,
      text: 'reds',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
    {
      key: 1,
      text: 'woodys',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
  ],
  Outdoors: [
    {
      key: 0,
      text: 'reds',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
    {
      key: 1,
      text: 'woodys',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
  ],
  Concerts: [
    {
      key: 0,
      text: 'reds',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
    {
      key: 1,
      text: 'woodys',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
  ],
  Family: [
    {
      key: 0,
      text: 'reds',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
    {
      key: 1,
      text: 'woodys',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
  ],
  Other: [
    {
      key: 0,
      text: 'reds',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      route: 'Activity',
    },
    {
      key: 1,
      text: 'woodys',
      details: {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      },
      image: Woodys,
      route: 'Activity',
    },
  ],
};

export default function Discover({navigation, route}) {
  const {id} = route?.params;
  var routeName;
  data.Routes.forEach(subRoute => {
    if (subRoute.key === id) {
      routeName = subRoute.name;
    }
  });
  navigation.setOptions({
    title: routeName,
  });

  return (
    <ScrollView style={{backgroundColor: ' gray'}}>
      {data[routeName].map((item, index) => (
        <DiscoveryBox
          image={item.image}
          text={item.text}
          details={item?.details}
          color={item.color}
          navigation={navigation}
          route={item.route}
          id={item.key}
        />
      ))}
    </ScrollView>
  );
}
