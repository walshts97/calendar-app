/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  NativeModules,
  ScrollView,
} from 'react-native';
import {Container, Icon} from 'native-base';
import Woodys from '../../media/UncleWoodys.jpg';
import AddButton from '../components/AddButton';
class Activity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Callback: '',
    };
    this.onAdd = this.onAdd.bind(this);
    // this.addToCalendar = this.addToCalendar.bind(this);
  }

  // componentDidMount() {
  //   return fetch('')
  //     .then(response => response.json())
  //     .then(responseJson => {
  //       this.setState({isLoading: false, dataSource: responseJson.Activity});
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }

  // async load() {
  //   await ActivityLoader.testCallback(
  //     err => {
  //       console.log(err);
  //     },
  //     msg => {
  //       console.log(msg);
  //       return msg;
  //     },
  //   );
  // }

  // addToCalendar() {
  //   var callback = this.load();
  //   this.setState({Callback: callback});
  //   console.log(this.state.Callback);
  // }

  onAdd() {
    var item = {name: this.props.route.params.name};
    this.props.navigation.navigate('Item', {
      edit: true,
      item: item,
    });
  }

  render() {
    this.props.navigation.setOptions({
      title: this.props.route.params.name,
    });
    return (
      <Container>
        <View>
          <View style={{width: '100%', alignItems: 'center'}}>
            <Image source={Woodys} />
          </View>
        </View>
        <ScrollView>
          <View flexDirection="row">
            <Text style={{flex: 1}}>Address: </Text>
            <Text style={{flex: 3}}>
              {this.props.route.params?.details?.address}
            </Text>
          </View>
          <View flexDirection="row">
            <Text style={{flex: 1}}>Hours: </Text>
            <Text style={{flex: 3}}>
              {this.props.route.params?.details?.hours}
            </Text>
          </View>
          <View flexDirection="row">
            <Text style={{flex: 1}}>Website: </Text>
            <Text style={{flex: 3}}>
              {this.props.route.params?.details?.website}
            </Text>
          </View>
          <View flexDirection="row">
            <Text style={{flex: 1}}>Reviews: </Text>
            <Text style={{flex: 3}}>
              {this.props.route.params?.details?.reviews}
            </Text>
          </View>
        </ScrollView>

        <AddButton icon={this.props.route.params.icon} onPress={this.onAdd} />
      </Container>
    );
  }
}
export default Activity;
