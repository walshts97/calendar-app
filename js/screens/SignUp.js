import React, {Component} from 'react';
import {Form, Item, Button, Text, Container, Input, Label} from 'native-base';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.signUp = this.signUp.bind(this);
  }

  signUp() {
    //api call and login
    var data =
      'username=' +
      this.state.username +
      '&email=' +
      this.state.email +
      '&password1=' +
      this.state.password1 +
      '&password2=' +
      this.state.password2 +
      '&csrfmiddlewaretoken=' +
      this.props.route.params.csrfToken;
    fetch('http://192.168.99.100:8000/auth/signup/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        Cookie: this.props.route.params.csrfToken,
      },
      body: data,
    })
      .then(response => {
        console.log(response);
        console.log(response.data);
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  }
  render() {
    return (
      <Container>
        <Form>
          <Item stackedLabel>
            <Label>Username</Label>
            <Input onChangeText={text => this.setState({username: text})} />
          </Item>
          <Item stackedLabel>
            <Label>Email</Label>
            <Input onChangeText={text => this.setState({email: text})} />
          </Item>
          <Item stackedLabel>
            <Label>Password</Label>
            <Input
              secureTextEntry={true}
              onChangeText={text => this.setState({password1: text})}
            />
          </Item>
          <Item stackedLabel secureTextEntry={true} last>
            <Label>Confirm Password</Label>
            <Input
              secureTextEntry={true}
              onChangeText={text => this.setState({password2: text})}
            />
          </Item>
          <Button onPress={() => this.signUp()}>
            <Text>Submit</Text>
          </Button>
        </Form>
      </Container>
    );
  }
}
