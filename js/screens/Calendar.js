import React, {Component} from 'react';
import {Agenda} from 'react-native-calendars';
import {StyleSheet} from 'react-native';

import {Container, View, Text} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ItemBox from '../components/ItemBox';
import AddButton from '../components/AddButton';
const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
});
export default class Calendar extends Component {
  constructor(params) {
    super(params);
    this.state = {
      SelectedDate: new Date(),
    };
    this.onAdd = this.onAdd.bind(this);
  }

  onAdd() {
    var item = {name: 'new event'};
    this.props.navigation.navigate('Item', {
      edit: true,
      item: item,
    });
  }

  render() {
    return (
      <Container>
        <Agenda
          // the list of items that have to be displayed in agenda. If you want to render item as empty date
          // the value of date key kas to be an empty array []. If there exists no value for date key it is
          // considered that the date in question is not yet loaded
          items={{
            '2020-04-20': [
              [
                {
                  name: 'Maleware Analysis',
                  date: '2020-04-20',
                  description: 'Lecture',
                  startTime: '12:00pm',
                  endTime: '1:00pm',
                },
                {
                  name: 'Meeting with Nick',
                  date: '2020-04-20',
                  description: 'Discuss authentication',
                  startTime: '1:30pm',
                  endTime: '2:30pm',
                },
                {
                  name: 'Dinner',
                  date: '2020-04-20',
                  description: 'Buffalo Wild Wings With the boys',
                  startTime: '5:30pm',
                  endTime: '7:30pm',
                },
              ],
            ],
            '2020-04-22': [
              [
                {
                  name: 'Network Security',
                  date: '2020-04-20',
                  description: 'Lecture',
                  startTime: '12:00pm',
                  endTime: '1:00pm',
                },
                {
                  name: 'Lunch With Kate',
                  date: '2020-04-20',
                  startTime: '1:30pm',
                  endTime: '2:30pm',
                },
              ],
            ],
          }}
          // callback that gets called when items for a certain month should be loaded (month became visible)
          loadItemsForMonth={(month, day) => {
            // console.log(month);
            // console.log(day);
          }}
          renderItem={(item, firstItemInDay) => {
            return (
              <View
                style={{
                  borderBottomColor: 'gray',
                  borderBottomWidth: 2,
                  paddingBottom: 5,
                }}>
                {item.map((event, index) => (
                  <ItemBox item={event} navigation={this.props.navigation} />
                ))}
              </View>
            );
          }}
          // callback that fires when the calendar is opened or closed
          onCalendarToggled={calendarOpened => {
            console.log(calendarOpened);
          }}
          // callback that gets called on day press
          onDayPress={day => {
            console.log('day pressed');
          }}
          // renderDay={(day, item) => {
          //   return <View />;
          // }}
          // callback that gets called when day changes while scrolling agenda list
          onDayChange={day => {
            console.log('day changed');
          }}
          // initially selected day
          selected={this.state.SelectedDate}
          // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
          minDate={'2018-05-10'}
          // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
          maxDate={'2022-05-30'}
          // Max amount of months allowed to scroll to the past. Default = 50
          pastScrollRange={24}
          // Max amount of months allowed to scroll to the future. Default = 50
          futureScrollRange={24}
          // specify your item comparison function for increased performance
          rowHasChanged={(r1, r2) => {
            return r1.text !== r2.text;
          }}
          // If provided, a standard RefreshControl will be added for "Pull to Refresh" functionality. Make sure to also set the refreshing prop correctly.
          onRefresh={() => console.log('refreshing...')}
          theme={{
            agendaDayTextColor: 'tomato',
            agendaDayNumColor: 'tomato',
            agendaTodayColor: 'cyan',
            agendaKnobColor: 'cyan',
            selectedDayBackgroundColor: 'tomato',
          }}
          style={{}}
        />
        <AddButton icon={this.props.route.params.icon} onPress={this.onAdd} />
      </Container>
    );
  }
}
