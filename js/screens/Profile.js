import React, {Component} from 'react';
import {Button, Text, Container} from 'native-base';

export default class Profile extends Component {
  render() {
    return (
      <Container>
        <Button onPress={() => this.props.route.params.signOutCallback()}>
          <Text>SignOut</Text>
        </Button>
      </Container>
    );
  }
}
