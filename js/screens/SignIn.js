import React, {Component} from 'react';
import {Form, Item, Input, Button, Text, Container} from 'native-base';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.Submit = this.Submit.bind(this);
    this.createAccount = this.createAccount.bind(this);
  }

  Submit() {
    //api call and login
    this.props.route.params.signInCallback();
  }
  createAccount() {
    console.log('create trigger');
    this.props.navigation.navigate('SignUp');
  }

  render() {
    return (
      <Container>
        <Form>
          <Item>
            <Input id="Email" placeholder="Email" />
          </Item>
          <Item last>
            <Input
              secureTextEntry={true}
              id="Password"
              placeholder="Password"
            />
          </Item>
          <Button onPress={() => this.Submit()}>
            <Text>Submit</Text>
          </Button>
        </Form>
        <Button onPress={() => this.createAccount()}>
          <Text>Create Account</Text>
        </Button>
      </Container>
    );
  }
}
