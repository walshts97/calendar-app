import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
} from 'native-base';

class HeaderBar extends Component {
  render() {
    return (
      <Header
        style={{
          backgroundColor: 'white',
          borderBottom: '2px',
          boderColor: 'black',
        }}>
        <Left style={{flex: 1}}>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate('Settings')}
            //   onPress={() => {
            // DrawerNav.dispatch(
            //   NavigationActions.reset({
            //     index: 0,
            //     actions: [NavigationActions.navigate({routeName: 'Home'})],
            //   }),
            // );
            // DrawerNav.goBack();
            //   }}>
          >
            <Icon active name="settings" style={{color: 'black'}} />
          </Button>
        </Left>

        <Body style={{flex: 2, alignItems: 'center'}}>
          <Title
            style={{
              color: 'black',
            }}>
            SoCalender <Icon name="calendar" />
          </Title>
        </Body>

        <Right style={{flex: 1}}>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate('Profile')}>
            <Icon style={{color: 'black'}} active name="person" />
          </Button>
        </Right>
      </Header>
    );
  }
}
export default HeaderBar;
