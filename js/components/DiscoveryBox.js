import React, {Component} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {Text} from 'native-base';
class DiscoveryBox extends Component {
  constructor(params) {
    super(params);
    this.state = {
      buttonStyle: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        margin: '2%',
        width: '96%',
        alignItems: 'center',
      },
      buttonLabel: {
        backgroundColor: this.props.Color,
        width: '100%',
        alignItems: 'center',
      },
    };
    this.onPress = this.onPress.bind(this);
  }

  onPress(route, id, name, details) {
    if (typeof details === 'undefined') {
      details = {
        address: '120 S Herbert Rd',
        hours: '10-2',
        website: '',
        reviews: [],
      };
    }
    this.props.navigation.navigate(route, {
      id: id,
      name: name,
      details: details,
    });
  }

  render() {
    return (
      <TouchableOpacity
        style={this.state.buttonStyle}
        onPress={() =>
          this.onPress(
            this.props.route,
            this.props.id,
            this.props.text,
            this.props?.details,
          )
        }>
        <Text style={{fontWeight: 'bold'}}>{this.props.text} </Text>
        <Image
          source={this.props.image}
          style={{width: '90%'}}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  }
}

export default DiscoveryBox;
