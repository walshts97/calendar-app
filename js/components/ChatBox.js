import React, {Component} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {Text} from 'native-base';

class ChatBox extends Component {
  constructor(params) {
    super(params);
    this.state = {
      buttonStyle: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        margin: '2%',
        width: '96%',
        flexDirection: 'row',
      },
    };
    this.onPress = this.onPress.bind(this);
  }

  onPress(name, id) {
    this.props.navigation.navigate('Conversation', {
      id: id,
      name: name,
    });
  }

  render() {
    return (
      <TouchableOpacity
        style={this.state.buttonStyle}
        onPress={() => this.onPress(this.props.Name, this.props.id)}>
        <Image source={this.props.Image} />
        <View>
          <Text style={{fontWeight: 'bold'}}>{this.props.Name} </Text>
          <Text>{this.props.Message}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ChatBox;
