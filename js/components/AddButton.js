//This is an example code for React Native Floating Action Button//
import React, {Component} from 'react';
//import react in our code.

import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Alert,
  Text,
} from 'react-native';

import {Icon} from 'native-base';
//import all the components we are going to use.

export default class AddButton extends Component {
  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        style={styles.TouchableOpacityStyle}
        onPress={() => this.props.onPress()}>
        <Icon style={{color: 'black'}} active name={this.props.icon} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    backgroundColor: 'cyan',
    borderRadius: 25,
    opacity: 0.5,
  },
});
