import React, {Component} from 'react';
import {View, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {Text} from 'native-base';
const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
});
class ItemBox extends Component {
  constructor(params) {
    super(params);
    this.onPress = this.onPress.bind(this);
  }

  onPress(item, date) {
    this.props.navigation.navigate('Item', {
      item: item,
    });
  }

  render() {
    return (
      <TouchableOpacity
        style={[styles.item]}
        onPress={() => this.onPress(this.props.item)}>
        <View style={{flexDirection: 'row'}}>
          <Text
            numberOfLines={1}
            style={{flex: 3, fontWeight: 'bold', margin: 1}}>
            {this.props.item.name}
          </Text>
          <Text style={{flex: 1, margin: 1}}>{this.props.item.startTime}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          {this.props.item.description ? (
            <Text numberOfLines={1} style={{flex: 3, margin: 1}}>
              {this.props.item?.description}
            </Text>
          ) : null}
          <Text style={{flex: 1, margin: 1}}>{this.props.item.endTime}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ItemBox;
