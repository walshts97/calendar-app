import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Icon, Container} from 'native-base';
import HeaderBar from '../components/HeaderBar.js';
import CalendarNav from './CalendarNav';
import DiscoverNav from './DiscoverNav';
import ChatNav from './ChatNav';

const Tab = createBottomTabNavigator();
export default function HomeNav({navigation}) {
  return (
    <Container>
      <HeaderBar navigation={navigation} />
      <Tab.Navigator
        initialRouteName="Calendar"
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            if (route.name === 'Calendar') {
              return <Icon name="calendar" style={{color: color}} />;
            } else if (route.name === 'Discover') {
              return <Icon name="search" style={{color: color}} />;
            } else if (route.name === 'Chat') {
              return <Icon name="text" style={{color: color}} />;
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'grey',
          activeBackgroundColor: 'cyan',
        }}>
        <Tab.Screen name="Calendar" component={CalendarNav} />
        <Tab.Screen name="Discover" component={DiscoverNav} />
        <Tab.Screen name="Chat" component={ChatNav} />
      </Tab.Navigator>
    </Container>
  );
}
