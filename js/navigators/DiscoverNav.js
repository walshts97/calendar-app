/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Container} from 'native-base';
import {createStackNavigator} from '@react-navigation/stack';
import Discover from '../screens/Discover';
import Activity from '../screens/Activity';
import Item from '../screens/Item';

const DiscoverStack = createStackNavigator();
class DiscoverNav extends Component {
  render() {
    return (
      <Container>
        <DiscoverStack.Navigator>
          <DiscoverStack.Screen
            name="Discover"
            component={Discover}
            initialParams={{id: 0}}
            options={{
              title: 'Discover',
              headerTitleAlign: 'center',
            }}
          />
          <DiscoverStack.Screen
            name="SubCategory"
            component={Discover}
            initialParams={{id: 0}}
            options={{
              headerTitleAlign: 'center',
              title: 'Category',
            }}
          />
          <DiscoverStack.Screen
            name="Activity"
            component={Activity}
            initialParams={{icon: 'add'}}
            options={{
              title: 'Activity',
              headerTitleAlign: 'center',
            }}
          />
          <DiscoverStack.Screen
            name="Item"
            component={Item}
            initialParams={{
              icon: 'add',
              editIcon: 'md-document',
              saveIcon: 'md-save',
            }}
            options={{
              title: 'Activity',
              headerTitleAlign: 'center',
            }}
          />
        </DiscoverStack.Navigator>
      </Container>
    );
  }
}
export default DiscoverNav;
