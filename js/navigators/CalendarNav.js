/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Container} from 'native-base';
import {createStackNavigator} from '@react-navigation/stack';
import Calendar from '../screens/Calendar';
import Item from '../screens/Item';

const CalendarStack = createStackNavigator();
class CalendarNav extends Component {
  render() {
    return (
      <Container>
        <CalendarStack.Navigator initialRouteName="Calendar">
          <CalendarStack.Screen
            name="Calendar"
            component={Calendar}
            initialParams={{id: 0, icon: 'add'}}
            options={{headerShown: false}}
          />
          <CalendarStack.Screen
            name="Item"
            component={Item}
            initialParams={{
              id: 0,
              editIcon: 'md-document',
              saveIcon: 'md-save',
            }}
            options={{title: 'Item'}}
          />
        </CalendarStack.Navigator>
      </Container>
    );
  }
}
export default CalendarNav;
