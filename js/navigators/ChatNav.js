/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Container} from 'native-base';
import {createStackNavigator} from '@react-navigation/stack';
import Message from '../screens/Message';
import Conversation from '../components/Conversation';

const ChatStack = createStackNavigator();
class ChatNav extends Component {
  render() {
    return (
      <Container>
        <ChatStack.Navigator>
          <ChatStack.Screen
            name="Messages"
            component={Message}
            options={{
              headerTitleAlign: 'center',
            }}
          />
          <ChatStack.Screen name="Conversation" component={Conversation} />
        </ChatStack.Navigator>
      </Container>
    );
  }
}
export default ChatNav;
