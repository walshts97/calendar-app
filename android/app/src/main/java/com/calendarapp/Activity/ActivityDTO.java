package com.calendarapp.Activity;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;

public class ActivityDTO extends ReactContextBaseJavaModule {
    public ActivityDTO(ReactApplicationContext reactContext){
        super(reactContext);
    }

    @Override
    public String getName(){
        return "TestActivity";
    }

    @ReactMethod
    public void testCallback(Callback errorCallback, Callback successCallback){
        try{
            System.out.println("working test");
            successCallback.invoke("Callback : Woodys");
        }
        catch (IllegalViewOperationException e){
            errorCallback.invoke(e.getMessage());
        }
    }
}
